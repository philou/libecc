.\" generated with Ronn-NG/v0.8.0
.\" http://github.com/apjanke/ronn-ng/tree/0.8.0
.TH "LIBECC\-ARM\-NONE\-EABI" "3" "December 2019" "" ""
.SH "NAME"
\fBlibecc\-arm\-none\-eabi\fR
.SH "Description"
This software implements a library for elliptic curves based cryptography (ECC)\. The API supports signature algorithms specified in the ISO 14888\-3:2016 \fI\%https://www\.iso\.org/standard/64267\.html?browse=tc\fR standard, with the following specific curves and hash functions:
.IP "\[ci]" 4
\fBSignatures\fR: ECDSA, ECKCDSA, ECGDSA, ECRDSA, EC{,O}SDSA, ECFSDSA\.
.IP "\[ci]" 4
\fBCurves\fR: SECP{224,256,384,521}R1, BRAINPOOLP{224,256,384,512}R1, FRP256V1, GOST{256,512}\. The library can be easily expanded with user defined curves using a standalone helper script\.
.IP "\[ci]" 4
\fBHash functions\fR: SHA\-2 and SHA\-3 hash functions (224, 256, 384, 512)\.
.IP "" 0
.P
Advanced usages of this library also include the possible implementation of elliptic curve based Diffie\-\-Hellman protocols as well as any algorithm on top of prime fields based elliptic curves (or prime fields, or rings of integers)\. Compared to other cryptographic libraries providing such features, the differentiating points are:
.IP "\[ci]" 4
A focus on code readability and auditability\. The code is pure C99, with no dynamic allocation and includes pre/post\-asserts in the code\. Hence, this library is a good candidate for embedded targets (it should be easily portable accross various platforms)\.
.IP "\[ci]" 4
A clean layer separation for all needed mathematical abstractions and operations\. Strong typing (as "strong" as C99 allows, of course) of mathematical objects has been used in each layer\.
.IP "\[ci]" 4
The library has NOT been designed to break performance records, though it does a decent job (see the \fI\%#performance\fR)\. Similarly, the library memory footprint (in terms of ROM and RAM usage) is not the smallest achievable one (though some efforts have been made to limit it and fit "common" platforms, see the \fI\%#constrained\-devices\fR)\.
.IP "\[ci]" 4
libecc library core has \fBno external dependency\fR (not even the standard libc library) to make it portable\. See the \fI\%#compatibility\-and\-portability\fR for more information\.
.IP "" 0
.SH "libecc library configuration"
By default, Debian version of libecc enables every curves, hash algorithms and schemes available in libecc:
.IP "" 4
.nf
    /* Supported curves */
    #define WITH_CURVE_FRP256V1
    #define WITH_CURVE_SECP192R1
    #define WITH_CURVE_SECP224R1
    #define WITH_CURVE_SECP256R1
    #define WITH_CURVE_SECP384R1
    #define WITH_CURVE_SECP521R1
    #define WITH_CURVE_BRAINPOOLP224R1
    #define WITH_CURVE_BRAINPOOLP256R1
    #define WITH_CURVE_BRAINPOOLP384R1
    #define WITH_CURVE_BRAINPOOLP512R1
    #define WITH_CURVE_GOST256
    #define WITH_CURVE_GOST512

    /* Supported hash algorithms */
    #define WITH_HASH_SHA224
    #define WITH_HASH_SHA256
    #define WITH_HASH_SHA384
    #define WITH_HASH_SHA512
    #define WITH_HASH_SHA3_224
    #define WITH_HASH_SHA3_256
    #define WITH_HASH_SHA3_384
    #define WITH_HASH_SHA3_512

    /* Supported sig/verif schemes */
    #define WITH_SIG_ECDSA
    #define WITH_SIG_ECKCDSA
    #define WITH_SIG_ECSDSA
    #define WITH_SIG_ECOSDSA
    #define WITH_SIG_ECFSDSA
    #define WITH_SIG_ECGDSA
    #define WITH_SIG_ECRDSA
.fi
.IP "" 0
.P
As already stated, libecc has not been designed with performance in mind, but with \fBsimplicity\fR and \fBportability\fR as guiding principles; this implies several things when it comes to performance:
.IP "\[ci]" 4
libecc does not intend to compete with libraries developed with platform specific accelerations, such as the use of \fBassembly\fR routines or the adaptation to CPUs quirks at execution time (e\.g\. a CPU with very slow shift instructions)\. OpenSSL \fI\%https://www\.openssl\.org/\fR is an example of such libraries with good and homogeneous performance in mind on most heterogeneous platforms (with the lack of portability on very small embedded platforms though)\.
.IP "\[ci]" 4
Some algorithmic tricks on specific prime curves are not implemented: the same algorithms are used for all the curves\. This means for instance that curves using pseudo\-Mersenne primes (such as NIST\'s SECP curves) won\'t be faster than curves using generic random primes (such as Brainpool curves), though pseudo\-Mersenne primes can benefit from a dedicated reduction algorithm, yielding \fBorders of magnitude faster field arithmetic\fR (around five to ten times faster)\. See here \fI\%https://tls\.mbed\.org/kb/cryptography/elliptic\-curve\-performance\-nist\-vs\-brainpool\fR for further discussions on this\. Consequently, we will only focus on performance comparison with other libraries using the Brainpool curves\.
.IP "\[ci]" 4
We use a very straightforward elliptic curve arithmetic implementation, without using literature generic algorithmic optimizations such as windowing \fI\%https://en\.wikipedia\.org/wiki/Elliptic_curve_point_multiplication#Windowed_method\fR or fixed\-base comb \fI\%https://link\.springer\.com/chapter/10\.1007/3\-540\-45537\-X_21\fR precomputations\.
.IP "" 0
.P
Nonetheless and despite all these elements, \fBlibecc is on par with some other general purpose and portable cryptographic libraries\fR such as mbedTLS \fI\%https://tls\.mbed\.org\fR (see the performance figures given below)\.
.P
We present hereafter the ECDSA performance comparison of libecc with mbedTLS and OpenSSL on various platforms representing different CPU flavours\. Here are some information about the tested version when not stated otherwise:
.IP "\[ci]" 4
mbedTLS: stable version 2\.4\.2, the figures have been gathered with the builtin benchmark\.
.IP "\[ci]" 4
OpenSSL: debian packaged version 1\.1\.0e\. Since OpenSSL builtin ECDSA benchmark does not handle Brainpool curves, a basic C code using "named curves" have been compiled against the installed dynamic library\.
.IP "" 0
.SS "Performance oriented platforms"
.IP "\[ci]" 4
\fBCore i7\-5500U\fR (Broadwell family) is a typical x86 mid\-range current laptop CPU\.
.IP "\[ci]" 4
\fBXeon E3\-1535M\fR (Skylake family) is a typical x86 high\-end CPU\.
.IP "\[ci]" 4
\fBPower\-7\fR is a typical server CPU of the previous generation (2010) with a PowerPC architecture\.
.IP "" 0
.P
For all the platforms in this subsection, the CPUs have been tested in 64\-bit mode\.
.IP "" 4
.nf
| **libecc**      | Core i7\-5500U @ 2\.40GHz     | Xeon E3\-1535M v5 @ 2\.90GHz    | Power\-7                   |
|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|
| BP256R1         | 583 sign/s \- 300 verif/s    | 700 sign/s \- 355 verif/s      | 213 sign/s \- 110 verif/s  |
| BP384R1         | 231 sign/s \- 118 verif/s    | 283 sign/s \- 150 verif/s      | 98 sign/s  \- 50 verif/s   |
| BP512R1         | 111 sign/s \- 56 verif/s     | 133 sign/s \- 68 verif/s       | 51 sign/s  \- 26 verif/s   |

| **mbedTLS**     | Core i7\-5500U @ 2\.40GHz     | Xeon E3\-1535M v5 @ 2\.90GHz    | Power\-7                   |
|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|
| BP256R1         | 426 sign/s \- 106 verif/s    | 552 sign/s \- 141 verif/s      | 178 sign/s \- 45 verif/s   |
| BP384R1         | 239 sign/s \- 56 verif/s     | 322 sign/s \- 77 verif/s       | 44 sign/s  \- 23 verif/s   |
| BP512R1         | 101 sign/s \- 26 verif/s     | 155 sign/s \- 34 verif/s       | 38 sign/s  \- 12 verif/s   |

| **OpenSSL**     | Core i7\-5500U @ 2\.40GHz     | Xeon E3\-1535M v5 @ 2\.90GHz    | Power\-7                   |
|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|
| BP256R1         | 2463 sign/s \- 1757 verif/s    | 2873 sign/s \- 2551 verif/s  | 1879 sign/s \- 1655 verif/s|
| BP384R1         | 1091 sign/s \- 966 verif/s     | 1481 sign/s \- 1265 verif/s  | 792 sign/s  \-  704 verif/s|
| BP512R1         | 727 sign/s \- 643 verif/s      | 1029 sign/s \- 892 verif/s   | 574 sign/s  \-  520 verif/s|
.fi
.IP "" 0
.SS "Embedded platforms with moderate constraints"
.IP "\[ci]" 4
\fBMarvel Armada A388\fR is a good representative of moderately constrained embedded devices, such as IAD (Internet Access Devices), NAS (Network Attached Storage), STB (Set Top Boxes) and smartphones\. This SoC is built around a Cortex\-A9 ARMv7\-A 32\-bit architecture\.
.IP "\[ci]" 4
\fBBCM2837\fR is a Broadcom SoC built around the recent 64\-bit ARMv8\-A architecture, with a Cortex\-A53 core\. This SoC can be found in the Raspberry Pi 3, and also represents what can be found in recent Smartphones\.
.IP "\[ci]" 4
\fBAtom D2700\fR is a small x86 CPU typically embedded in NAS devices\. Though its "embedded" coloration, it uses a 64\-bit mode that we have tested here\.
.IP "" 0
.IP "" 4
.nf
| **libecc**      | Marvell A388 @ 1\.6GHz | BCM2837 (aarch64) @ 1\.2GHz | Atom D2700 @ 2\.13GHz   |
|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|
| BP256R1         | 64 sign/s \- 33 verif/s| 43 sign/s \- 22 verif/s     | 68 sign/s \- 35 verif/s |
| BP384R1         | 24 sign/s \- 12 verif/s| 17 sign/s \- 9 verif/s      | 25 sign/s \- 13 verif/s |
| BP512R1         | 11 sign/s \- 5 verif/s | 8 sign/s \- 4 verif/s       | 12 sign/s \- 6 verif/s  |

| **mbedTLS**     | Marvell A388 @ 1\.6GHz | BCM2837 (aarch64) @ 1\.2GHz | Atom D2700 @ 2\.13GHz   \-|
|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|
| BP256R1         | 33 sign/s \- 8 verif/s   | 14 sign/s \- 4 verif/s      | 87 sign/s \- 22 verif/s|
| BP384R1         | 20 sign/s \- 4 verif/s   | 8 sign/s \- 2 verif/s       | 50 sign/s \- 11 verif/s|
| BP512R1         | 10 sign/s \- 2 verif/s   | 4 sign/s \- 1 verif/s       | 23 sign/s \- 5 verif/s |

| **OpenSSL**     | Marvell A388 @ 1\.6GHz   | BCM2837 (aarch64) @ 1\.2GHz |  Atom D2700 @ 2\.13GHz   |
|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|
| BP256R1         | 369 sign/s \- 332 verif/s| 124 sign/s \- 112 verif/s   | 372 sign/s \- 334 verif/s|
| BP384R1         | 102 sign/s \- 94 verif/s | 54 sign/s \- 49 verif/s     | 163 sign/s \- 149 verif/s|
| BP512R1         | 87 sign/s \- 81 verif/s  | 31 sign/s \- 29 verif/s     |  92 sign/s \- 83 verif/s |
.fi
.IP "" 0
.SS "Very constrained embedded devices"
The library, when configured for a 256\-bit curve (SECP256R1, FRP256), SHA\-256 and ECDSA signature fits in around \fB30 Kilo Bytes of flash/EEPROM\fR, and uses around \fB8 Kilo Bytes of RAM\fR (stack) with variations depending on the chosen WORDSIZE (16, 32, 64), the compilation options (optimization for space \fB\-Os\fR or speed \fB\-O3\fR) and the target (depending on the instructions encoding, produced binary code can be more or less compact)\. A 521\-bit curve with SHA\-256 hash function and ECDSA signature should fit in 38 Kilo Bytes of flash and around 16 Kilo Bytes of RAM (stack), with the same variations depending on the WORDSIZE and the compilation options\.
.P
\fBNote\fR: libecc does not use any heap allocation, and the only global variables used are the \fBconstant ones\fR\. The constant data should end up in the flash/EEPROM section with a read only access to them: no RAM memory should be consumed by these\. The libecc read/write data are only made of local variables on the stack\. Hence, RAM consumption (essentially made of arrays representing internal objects such as numbers, point on curves \|\.\|\.\|\.) should be reasonably constant across platforms\.
.P
\fBHowever\fR, some platforms using the \fBHarvard architecture \fI\%https://en\.wikipedia\.org/wiki/Harvard_architecture\fR\fR (as opposed to Von Neumann\'s one) can have big limitations when accessing so called "program memory" as data\. The 8\-bit Atmel AVR \fI\%http://www\.atmel\.com/products/microcontrollers/avr/\fR MCU is such an example\. Compilers and toolchains for such architectures usually copy read only data in RAM at run time, and/or provide non\-standard ways \fI\%http://www\.atmel\.com/webdoc/avrlibcreferencemanual/pgmspace_1pgmspace_strings\.html\fR to access read only data in flash/EEPROM program memory (through specific macros, pragmas, functions)\. The first case means that the RAM consumption will increase for libecc compared to the stack only usage (because of the runtime copy)\. The second case means that libecc code will have to be \fBadapted\fR to the platform if the user want to keep RAM usage at its lowest\. In any case, tracking where \fBconst\fR qualified data reside will be important when the amount of RAM is a critical matter\.
.P
A full software stack containing a known test vector scenario has been compiled and tested on a \fBCortex\-M0\fR (STM32F030R8T6 \fI\%http://www\.st\.com/en/microcontrollers/stm32f030r8\.html\fR @ 48MHz with 64KB of flash and 8KB of RAM)\.
.P
It has also been compiled and tested on a \fBCortex\-M3\fR (STM32F103C8T6 \fI\%http://www\.st\.com/en/microcontrollers/stm32f103c8\.html\fR @ 72MHz with 64KB of flash and 20KB of RAM)\. The results of the flash/RAM occupancy are given in the table below, as well as the timings of the ECDSA signature and verification operations\.
.P
\fBNote\fR: The Cortex\-M0 case is a bit special in the ARM family\. Since this MCU lacks a 32\-bit x 32\-bit to 64\-bit multiplication instruction, the multiplication is implemented using a builtin software function\. This yields in poor performance with WORDSIZE=64 compared to WORDSIZE=32 (this might be explained by the calling cost to the builtin function)\.
.IP "" 4
.nf
| **libecc**      | STM32F103C8T6 (Cortex\-M3 @ 72MHz) | STM32F030R8T6 (Cortex\-M0 @ 48MHz) |
|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|
| Flash size      |           32KB                    |       30KB                        |
| RAM size        |           8KB                     |       8KB                         |
| Sign time       |           950ms                   |       2146ms                      |
| Verif time      |           1850ms                  |       4182ms                      |
.fi
.IP "" 0
.P
In order to compare the libecc performance on these embedded platforms, we give figures for mbedTLS on Cortex\-M3 taken from a recent study by ARM \fI\%http://csrc\.nist\.gov/groups/ST/lwc\-workshop2015/presentations/session7\-vincent\.pdf\fR\. As we have previously discussed, only the figures without NIST curves specific optimizations are of interest for a fair comparison:
.IP "" 4
.nf
| **mbedTLS**     | LPC1768 (Cortex\-M3 @ 92MHz)^1 |
|\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|:\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-|
| Flash size      |         ??                    |
| RAM size        |         3KB^2                 |
| Sign time       |         1893ms                |
| Verif time      |         3788ms                |
.fi
.IP "" 0
.IP "1." 4
Beware of the MCU frequency difference when comparing with libecc test case\.
.IP "2." 4
This figure only includes heap usage (stack usage is unknown so this is only a rough lower limit for RAM usage)\.
.IP "" 0
.SH "libecc and constant time"
Though \fBsome efforts\fR have been made to have (most of) the core algorithms constant time, turning libecc into a library shielded against side channel attacks is still a \fBwork in progress\fR\.
.P
Beyond pure algorithmic considerations, many aspects of a program can turn secret leakage resistance into a very complex problem, especially when writing portable C code\. Among other things, we can list the following:
.IP "\[ci]" 4
Low level issues can arise when dealing with heterogeneous platforms (some instructions might not be constant time) and compilers optimizations (a C code that seems constant time is in fact compiled to a non constant time assembly)\.
.IP "\[ci]" 4
Any shared hardware resource can become a leakage source (the caches, the branch prediction unit, \|\.\|\.\|\.)\. When dealing with a portable source code meant to run on most platforms, it is not an easy task to think of all these leakage sources\.
.IP "" 0
.P
For a thorough discussion about cryptography and constant time challenges, one can check this page \fI\%https://bearssl\.org/constanttime\.html\fR\.
